#ifndef MODEFORM_H
#define MODEFORM_H

#include <QWidget>

namespace Ui {
class ModeForm;
}

class ModeForm : public QWidget
{
    Q_OBJECT

public:
    explicit ModeForm(QWidget *parent = 0);
    ~ModeForm();

private:
    Ui::ModeForm *ui;


signals:
    void autoPressed(bool);
};

#endif // MODEFORM_H
