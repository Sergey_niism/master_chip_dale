#include "widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    connect (btnModTechnologic, SIGNAL(toggled(bool)), this, SLOT(technologicToggle(bool)));
    connect (this, SIGNAL(setPanelMode(QWidget*)),stWgtModes, SLOT(setCurrentWidget(QWidget*)));

    connect (pgModes, SIGNAL(autoPressed(bool)),pgChannels, SLOT(resetButtons(bool)));
}

Widget::~Widget()
{

}

void Widget::technologicToggle(bool checked)
{
 if (checked) emit setPanelMode(pgChannels);
 else emit setPanelMode(pgModes);
}
