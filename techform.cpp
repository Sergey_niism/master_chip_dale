#include "techform.h"
#include "ui_techform.h"

TechForm::TechForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TechForm)
{
    ui->setupUi(this);


}

TechForm::~TechForm()
{
    delete ui;
}

void TechForm::resetButtons(bool state)
{
    ui->pushButton_fi->setChecked(state);
    ui->pushButton_gama->setChecked(state);
    ui->pushButton_teta->setChecked(state);
    ui->pushButton_X->setChecked(state);
    ui->pushButton_Y->setChecked(state);
    ui->pushButton_Z->setChecked(state);
}
