#ifndef TECHFORM_H
#define TECHFORM_H

#include <QWidget>

namespace Ui {
class TechForm;
}

class TechForm : public QWidget
{
    Q_OBJECT

public:
    explicit TechForm(QWidget *parent = 0);
    ~TechForm();

private:
    Ui::TechForm *ui;


private slots:
    void resetButtons(bool state);


};

#endif // TECHFORM_H
