#include "modeform.h"
#include "ui_modeform.h"

ModeForm::ModeForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ModeForm)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(toggled(bool)),this,SIGNAL(autoPressed(bool)));
}

ModeForm::~ModeForm()
{
    delete ui;
}
